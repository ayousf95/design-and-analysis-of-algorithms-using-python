# Primality Testing A Monte Carlo Algorithm
#Amir Yousf
#Date : 25/10/2017

import random
import math

large = 10

def prime( n ):
    q = n - 1
    for i in range (1, large):
        m = q
        y = 1
        a = random.randint(1,1000)
        a = a % n
        z = a
        # compute a ^ n-1
        while (m > 0):
            while (m % 2 == 0):
                z = ((z * z) % n)
                m = math.floor(m/2)
            m = m - 1
            y = ( ( y * z) % n)
            
        if ( y != 1):
            print(" [ %d ] is not prime " %n)
            break
        else:
            print(" [ % d ] is prime" %n)
            break

num = int(input("Enter a number to check if it is prime or not :"))
prime(num)
