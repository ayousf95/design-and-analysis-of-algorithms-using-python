#Implementation of eight queens problem in Python
#Backtracking
#Amir Yousf
#Date :30/12/2017

list = []

def place(k, i):
    for j in range(1, k-1):
        if((list[j] == i) or (abs(list[j] -i )== abs(j - k))):
            return 0
    return 1

def nqueens(k,n):
    for i in range(1,n):
        if(place(k,i) == 1):
            list.append(i);
        if(k==n):
            print(list)
        else:
            nqueens(k+1, n)

nqueens(1,4)
