#Minimum and Maximum of array Implemenation
# Amir Yousf
# Date : 18/10/2017

nums = []


def max(nums, left, right):
    if left == right:
        return nums[left]
    else:
        mid = (left + right) / 2
        max1 = max(nums, left, mid)
        max2 = max(nums, mid+1, right)
        return max1 if max1 > max2 else max2
   
def main():
    li = [1, 5, 2, 9, 3, 7, 5, 2, 10]
    print ("Maximum element of the list is", max(nums, 0, 8))
    
    
if __name__ == "__main__":
    main()

   
"""n = int(input("Enter the maximum no of elements to be stored :"))

print("Enter the numbers one by one ")
for i in range(0, n):
    x = int(input("Enter the element :"))
    nums.append( x )

print ("Maximum element of the list is", max(nums, 0, len(nums)-1))"""
